import { config } from 'dotenv'
import { Client } from 'pg'

config()
export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});


