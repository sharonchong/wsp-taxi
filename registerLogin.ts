import { Application } from 'express';
import { client } from './db';
import { hashPassword, checkPassword } from './hash';
import { upload } from './upload'
import './session'
import { checkRegister} from './guard';



export function registerLogin(app: Application) {
    app.post('/login/register', async (req, res) => {

    
        let user = req.body
        // for(let i = 0; i < user.phoneNum.length; i++){
        //     if(user.phoneNum[i] != types of Number){
        //         res.status(400).json({ message: 'Phone Number invalid' })
        //     return
        //     }
        // } 

        if (user.phoneNum.length != 8) {
            res.status(400).json({ message: 'Phone Number invalid' })
            return
        }

        if (user.phoneNum[0] == '1' || user.phoneNum[0] == '2' || user.phoneNum[0] == '3' || user.phoneNum[0] == '8' || user.phoneNum[0] == '0') {
            res.status(400).json({ message: 'Phone Number invalid' })
            return
        }

        if (user.password.length < 8) {
            res.status(400).json({ message: 'Password length at less than 8 characters' })
            return
        }

        if (user.password != user.matchPassword) {
            res.status(400).json({ message: 'Password mismatch' })
            return
        }
        const time = new Date

        let phoneNumResult = (await client.query('select phone_num from users where users.phone_num = $1', [user.phoneNum])).rows
        if (phoneNumResult.length == 1) {
            console.log(phoneNumResult)
            res.status(400).json({ message: 'Phone number already exist' })
            return
        }

        let userId = (await client.query('INSERT INTO users(phone_num,password_hash,created_at) VALUES ($1,$2,$3) returning users.id', [user.phoneNum, await hashPassword(user.password), time])).rows[0]
        req.session.user = {
            user_id: userId.id,
            //to be confirm save phoneNum in session
            phone_num: user.phoneNum
        }
        // req.session.is_login = true
        req.session.save()
        res.json({ message: 'success' })

    })

    app.post('/login/signIn', async (req, res) => {

        const phoneNum = req.body.phoneNum
        const password = req.body.password

        let phoneNumResult = await client.query('select id as user_id, phone_num, password_hash from users where users.phone_num = $1', [phoneNum])
        if (phoneNumResult.rowCount == 0) {
            res.status(400).json({ message: 'Phone number not exist' })
            return
        }

        let { user_id, phone_num, password_hash } = phoneNumResult.rows[0]

        if (await checkPassword(password, password_hash)) {

            let driver_id = (await client.query('select id from driver where user_id = $1', [user_id])).rows[0]?.id

            let traveller_id = (await client.query('select id from traveller where user_id = $1', [user_id])).rows[0]?.id

            // console.log(travellerData)
            // let idContainer = driverData.length === 1 ?
            //     {
            //         driver_id: driverData[0].id,
            //         is_driver: true,
            //         user_id: phoneNumResult.id,
            //         phone_num: phoneNumResult.phoneNum
            //     } :
            //     {
            //         traveller_id: travellerData[0].id,
            //         is_driver: false,
            //         user_id: phoneNumResult.id,
            //         phone_num: phoneNumResult.phoneNum
            //     };

            req.session.user = {
                user_id,
                phone_num,
                driver_id,
                traveller_id
            }
            // req.session.is_login = true
            req.session.save()

            res.status(200).json({
                message: "Login success",
                is_driver: !!driver_id,
                is_traveller: !!traveller_id,
            })
        } else {
            res.status(400).json({ message: 'Password incorrect' })

        }
    })
    app.post('/profile/driver', checkRegister, upload.single('license'), async (req, res) => {

        try {
            // if(!req.file?.filename){
            //     res.status(403).json({ message: 'no license file'})
            //     return
            // }
            console.log('post driver profile')
            let user_id = req.session.user?.user_id
            let { first_name, last_name, phone_num } = req.body
            let driver_license_image = req.file?.filename || req.body.driver_license_image


            await client.query(/* sql */`
              update users
              set phone_num = $1
              where id = $2
            `, [phone_num, user_id])

            let result = await client.query(/* sql */`
              select
                id
              from driver
              where user_id = $1
            `, [user_id])

            if (result.rowCount != 0) {
                let driver_id = result.rows[0].id
                await client.query(/* sql */`
                update driver
                set (first_name, last_name,driver_license_image) = ($1,$2,$3)
                where id = $4
              `, [first_name, last_name, driver_license_image, driver_id])
                res.json({ message: 'updated driver profile' })
                return
            }

            result = await client.query(/* sql */`
              insert into driver
              (user_id,first_name,last_name,driver_license_image)
              values($1,$2,$3,$4)
              returning id`,
                [user_id, first_name, last_name, driver_license_image])
            let driver_id = result.rows[0].id

            req.session.user!.driver_id = driver_id;
            req.session.save()


            res.json({ message: 'created driver profile' })


            // let driver = req.body
            // console.log(driver)
            // let driverObj = {
            //     firstName: driver.first_name,
            //     lastName: driver.last_name,
            //     userID: req.session.user!.user_id,
            //     driverLicense: req.file!.filename

            // }
            // let id = (await client.query('insert into driver(user_id,first_name,last_name,driver_license_image) values($1,$2,$3,$4) returning driver.id ', [driverObj.userID, driverObj.firstName, driverObj.lastName, driverObj.driverLicense])).rows[0].id

            // req.session.user!.driver_id = id;
            // // req.session.user!.is_driver = true;
            // // req.session.is_login = true
            // req.session.save()
            // console.log(req.session)
            // res.json({ message: 'success' })
            // res.redirect('/driverOrder.html')
        } catch (error: any) {
            console.log('hello', error)
            res.status(500).json({ error: error.toString() })
        }

    })
    app.get('/profile/driver', checkRegister, async (req, res) => {
        try {
            console.log('get ran')
            let user_id = req.session.user!.user_id
            console.log('print ou the user id', user_id)
            console.log('start querying the sql')


            let result = await client.query(/* sql */`
              select
                first_name
              , last_name
              , phone_num
              , driver_license_image
              , gender
              from driver
              inner join users on users.id = user_id
              left join gender on users.gender_id = gender.id
              where user_id = $1
            `, [user_id])

            /* check driver table if the user_id exits
             if not,then create a default profile
            */

            if (result.rowCount != 0) {
                let profile = result.rows[0]
                profile.user_id = user_id
                res.json(profile)
                return
            }

            result = await client.query(/* sql */`
              select
                phone_num
              from users
              where id = $1
            `, [user_id])

            res.json({
                user_id: user_id,
                phone_num: result.rows[0].phone_num,
                first_name: "",
                last_name: "",
                driver_license_image: ""
            })
            // /* check driver table if the user_id exits
            //     if not,then dont join query as the user is new
            // */
            // let flag = await client.query(/* sql */

            //     `select exists(select 1 from driver where user_id = $1)`, [user_id])

            // console.log('print result of query', flag.rows[0].exists)
            // if (flag.rows[0].exists) {
            //     console.log('entered');

            //     let result = await client.query(/* sql */`
            //     select
            //     first_name
            //     , last_name
            //     , phone_num
            //     , driver_license_image
            //     , gender
            //     from driver
            //     inner join users on users.id = user_id
            //     left join gender on users.gender_id = gender.id
            //     where user_id = $1
            //     `, [user_id])
            //     let profile = result.rows[0]
            //     console.log('profile :', profile)
            //     profile.user_id = user_id
            //     res.json(profile)
            //     return

            // } else {
            //     let user_id = req.session.user!.user_id
            //     let phoneNumResult = (await client.query('select * from users where id = $1', [user_id])).rows[0].phone_num

            //     let profile = {
            //         user_id: `${user_id}`,
            //         phone_num: phoneNumResult,
            //         first_name: "",
            //         last_name: "",
            //         driver_license_image: ""
            //     }
            //     res.json(profile)
            //     return
            // }


        } catch (error: any) {
            console.log('bye', error);
            res.status(500).json({ error: error.toString() })
        }

    })
    app.post('/profile/traveller', checkRegister,async (req, res) => {
        try {
            let user_id = req.session.user?.user_id
            let { first_name, last_name, phone_num } = req.body


            await client.query(/* sql */`
            update users
            set phone_num = $1
            where id = $2
          `, [phone_num, user_id])

            let result = await client.query(/* sql */`
            select
              id
            from traveller
            where user_id = $1
          `, [user_id])


            if (result.rowCount != 0) {
            let traveller_id = result.rows[0].id
            await client.query(/* sql */`
            update traveller
            set (first_name, last_name) = ($1,$2)
            where id = $3
          `, [first_name, last_name, traveller_id])
                res.json({ message: 'updated traveller profile' })
                return
            }

            result = await client.query(/* sql */`
              insert into traveller
              (user_id,first_name,last_name)
              values($1,$2,$3)
              returning id`,
                [user_id, first_name, last_name])
            let traveller_id = result.rows[0].id

            console.log(result)
            req.session.user!.traveller_id = traveller_id;
            req.session.save()


            res.json({ message: 'created traveller profile' })

        } catch (error: any) { 
            res.status(500).json({ error: error.toString() }) 
        }
        // let traveller = req.body
        // let id = (await client.query('insert into traveller(user_id,first_name,last_name) values($1,$2,$3) returning traveller.id', [req.session.user!.user_id, traveller.firstName, traveller.lastName])).rows[0].id
        // req.session.user!.traveller_id = id;
        // // req.session.user!.is_driver = false;
        // // req.session.is_login = true
        // req.session.save()

        // console.log(req.session)

        // res.json({ message: 'success' })
    }
    )
    app.get('/profile/traveller',checkRegister, async (req, res) => {
        try {
            console.log('get ran')
            let user_id = req.session.user!.user_id
            console.log('print ou the user id', user_id)
            console.log('start querying the sql')

            let result = await client.query(/* sql */`
            select
              first_name
            , last_name
            , phone_num
            , gender
            from traveller
            inner join users on users.id = user_id
            left join gender on users.gender_id = gender.id
            where user_id = $1
          `, [user_id])

          /* check driver table if the user_id exits
           if not,then create a default profile
          */

          if (result.rowCount != 0) {
              let profile = result.rows[0]
              profile.user_id = user_id
              res.json(profile)
              return
          }

          result = await client.query(/* sql */`
            select
              phone_num
            from users
            where id = $1
          `, [user_id])

          res.json({
              user_id: user_id,
              phone_num: result.rows[0].phone_num,
              first_name: "",
              last_name: "",
          })


        } catch (error: any) {
            console.log('bye', error);
            res.status(500).json({ error: error.toString() })
        }
    })
    app.get('/user/logout', async (req, res) => {
        req.session.user = undefined
        res.json('user logout')
    })

}

