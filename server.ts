import express from 'express'
import path from 'path'
import { print } from 'listening-on'
import { loadLocation } from './user'
import { registerLogin } from './registerLogin'
import { driverPage } from './driverOrder'
import { attachSession } from './session'
// import {checkRegister,checkSignInDriver,checkSignInTraveller}from './guard'
import { client } from './db'
import { joinRoomPage } from './joinRoom'
import { Server as SocketIOServer } from 'socket.io'
import { Server as HttpServer } from 'http'
import { orderRoom } from './orderRoom'


let app = express()
client.connect()

app.use(express.json())

let httpServer = new HttpServer(app)
export let socketIoServer = new SocketIOServer(httpServer)
socketIoServer.on('connection', socket => {
	console.log('socket connected:', socket.id)
	socket.on('/room/join', id => {
		console.log(id)
	})
})

app.use((req, res, next) => {
	console.log(req.method, req.url);
	next()
})

attachSession(app)
registerLogin(app)
loadLocation(app)
driverPage(app, socketIoServer)
joinRoomPage(app)
orderRoom(app, socketIoServer)

app.use('/uploads', express.static('uploads'))
app.use(express.static('public'))

app.use((req, res) => {
	res.status(404).sendFile(path.resolve(path.join('public', '404.html')))
})

let port = 8100

httpServer.listen(port, () => {
	print(port)
})

