import { Application } from 'express'
import { client } from './db';
import { checkSignInTraveller } from './guard';
import { socketIoServer } from './server';
import './session'
const fetch = require('node-fetch')



export function loadLocation(app: Application) {
    function nowTime() {
        let date = new Date()
        let newSecond;
        if (date.getSeconds.length == 1) {
            let second = date.getSeconds()
            newSecond = '0' + second
        } else {
            newSecond = date.getSeconds()
        }
        let newDate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + newSecond
        return newDate
    }
    
    app.get('/user', checkSignInTraveller, async (req, res) => {
        res.json('login success')
    })

    app.get('/checkUserIsLogin', async (req, res) => {

        if(req.session.user){

            if(req.session.user?.driver_id){
                res.json({login : true, role: "driver"})

            }
            else if (req.session.user?.traveller_id){
                res.json({login : true, role: "traveller"})
            }
            else {
                res.json({login : true, role: ""})
            }

        }
        else{
            res.json({login : false})
        }
    })

    app.post('/google/address', async (req, res) => {
        let latlng = req.body
        let googleRes = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng.lat},${latlng.lng}&key=${process.env.GOOGLE_API_KEY}`)
        let json = await googleRes.json()
        res.json({
            address: json.results[0].formatted_address,
            lat: latlng.lat,
            lng: latlng.lng
        })
    })

    app.post('/user/requirements', async (req, res) => {
        try {
            let form = req.body

            let locationID = (await client.query('insert into location (latlng,name) values ($1,$2),($3,$4) returning location.id', [`${form.pickup.lat},${form.pickup.lng}`, form.pickup.address, `${form.dropOff.lat},${form.dropOff.lng}`, form.dropOff.address])).rows

            let requestID = (await client.query(`
            insert into request (pickup_location,drop_off_location,female_only,male_only,passenger_limit,created_at,red_tunnel,west_tunnel,east_tunnel,any_tunnel,planned_apart_time)
            values
            ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) returning request.id
            `, [locationID[0].id, locationID[1].id, form.female, form.male, form.passengerLimit, nowTime(), form.redTunnel, form.westTunnel, form.eastTunnel, form.anyTunnel, nowTime()])).rows[0]

            await client.query('insert into passenger (request_id,user_id,created_at) values ($1,$2,$3)', [requestID.id, req.session.user!.user_id, nowTime()])
            // io.emit('',{})



            socketIoServer.emit('request', {
                request_id: requestID,
                passenger_limit: form.passengerLimit,
                pickup: {
                    lat: form.pickup.lat,
                    lng: form.pickup.lng,
                    name: form.pickup.address
                },
                dropOff: {
                    lat: form.dropOff.lat,
                    lng: form.dropOff.lng,
                    name: form.dropOff.address
                },
                red_tunnel: form.redTunnel,
                west_tunnel: form.westTunnel,
                east_tunnel: form.eastTunnel,
                any_tunnel: form.anyTunnel
            })
            res.status(200).json({ request: requestID.id, message: 'Order created successfully' })
        }
        catch (e) {
            console.log(e)
            res.status(500).json({ request: -1, message: 'Order created failed' })

        }
    })

    app.get('/user/orderRoute', checkSignInTraveller, async (req, res) => {
        try {
            let request_id = req.query.request_id

            let address = (await client.query(`select
            pickup_location.latlng pickup_location_latlng
            , drop_off_location.latlng drop_off_location_latlng
            
            from request
            inner join location pickup_location on request.pickup_location = pickup_location.id
            inner join location drop_off_location on request.drop_off_location = drop_off_location.id
            where request.id = $1;`, [request_id])).rows[0]
            res.json(address)
        } catch (error) {
            console.log(error)
        }
    })
}

