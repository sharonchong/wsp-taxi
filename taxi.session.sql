create database taxi;

create user taxi with password 'taxi' superuser;

\c taxi;
create table gender (
    id serial primary key
    ,gender varchar(256) not null
);

create table "users" (
    id serial primary key
    ,phone_num CHAR(8) not null
    ,password_hash CHAR(60) not null
    ,gender_id integer 
    ,created_at TIMESTAMP not null
    ,updated_at timestamp
    ,foreign key (gender_id) REFERENCES gender(id)
);

create table "location" (
    id serial primary key
    ,latlng point 
    ,name varchar(255) not null
);

create table traveller (
    id serial primary key
    ,user_id integer not null
    ,pickup_address integer
    ,drop_off_address integer 
    ,foreign key (user_id) REFERENCES users(id)
    ,foreign key (pickup_address) REFERENCES "location"(id)
    ,foreign key (drop_off_address) REFERENCES "location"(id)
    ,on_car boolean 
    ,first_name varchar(256) 
    ,last_name varchar(256)
);

create table driver (
    id serial primary key
    ,user_id integer not null
    ,location_id integer
    ,foreign key (user_id) REFERENCES users(id)
    ,driver_license_image varchar (256) not NULL
    ,foreign key (location_id) REFERENCES "location"(id)
    ,first_name varchar(256) not NULL
    ,last_name varchar(256) not NULL
);

create table request (
    id serial primary key
    ,driver_id integer
    ,pickup_location integer not null
    ,drop_off_location integer not null
    ,driver_location integer
    ,foreign key (driver_id) REFERENCES driver(id)
    ,foreign key (pickup_location) REFERENCES "location"(id)
    ,foreign key (drop_off_location) REFERENCES "location"(id)
    ,foreign key (driver_location) REFERENCES "location"(id)
    ,female_only boolean not null
    ,male_only boolean not null
    ,passenger_limit INTEGER not null
    ,red_tunnel boolean not null
    ,west_tunnel boolean not null
    ,east_tunnel boolean not null
    ,any_tunnel boolean not null
    ,planned_apart_time TIMESTAMP not null
    ,cancel_time timestamp
    ,apart_time timestamp
    ,depart_time timestamp
    ,created_at timestamp not null
);

select * from request;





create table request_gender (
    id serial primary key
    ,request_id integer not null
    ,gender_id integer not null
    ,foreign key (request_id) REFERENCES request(id)
    ,foreign key (gender_id) REFERENCES gender(id)
);

create table passenger (
    id serial primary key
    ,request_id integer not null
    ,user_id integer not null
    ,foreign key (request_id) REFERENCES request(id)
    ,foreign key (user_id) REFERENCES users(id)
    ,created_at timestamp not null
);

create table feedback (
    id serial primary key
    ,passenger_id integer not null
    ,foreign key (passenger_id) REFERENCES passenger(id)
    ,comment varchar(256) not NULL
    ,score integer not NULL
    ,created_at timestamp not null
);



////////////////////////////////test
ALTER TABLE request ALTER COLUMN driver_location drop not null;
ALTER TABLE driver add COLUMN first_name varchar(256) not null;
ALTER TABLE driver add COLUMN last_name varchar(256) not null;

select * from driver;

ALTER TABLE traveller add COLUMN first_name varchar(256) not null;
ALTER TABLE traveller add COLUMN last_name varchar(256) not null;
ALTER TABLE traveller ALTER COLUMN on_car drop not null;
ALTER TABLE traveller ALTER COLUMN last_name  drop not null;

ALTER TABLE traveller ALTER COLUMN pickup_address drop not null;
ALTER TABLE traveller ALTER COLUMN drop_off_address drop not null;
ALTER TABLE traveller ALTER COLUMN pickup_address drop not null;
ALTER TABLE passenger modify COLUMN id serial primary key;

-- Drop TABLE pasenger;




-- add make user_id of driver unique

update request set driver_id = null;
delete from driver;
alter table driver add constraint driver_user_id_uniq unique (user_id);
delete from traveller;
alter table traveller add constraint traveller_user_id_uniq unique (user_id);