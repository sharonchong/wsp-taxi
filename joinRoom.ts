import { Application } from 'express'
import { client } from './db'
import { checkSignInTraveller } from './guard'

export function joinRoomPage(app: Application) {
    app.get('/joinOrder', checkSignInTraveller,async function (req, res) {
        try {
            let result = await client.query(/* sql */ `
                select
                request.id request_id
                , request.passenger_limit passenger_limit 
                , request.created_at
                , request.red_tunnel red_tunnel
                , request.west_tunnel west_tunnel
                , request.east_tunnel east_tunnel
                , request.any_tunnel any_tunnel
                , pickup_location.name pickup_location_name
                , pickup_location.latlng pickup_location_latlng
                , drop_off_location.name drop_off_location_name
                , drop_off_location.latlng drop_off_location_latlng
                
                from request
                inner join location pickup_location on request.pickup_location = pickup_location.id
                inner join location drop_off_location on request.drop_off_location = drop_off_location.id
                where driver_id is null 
            `)

            res.json(result.rows)
        } catch (error: any) {
            res.status(500).json({ error: error.toString() })
        }
    })
    app.post(
        '/joinOrder/:id/pickOrder',
        checkSignInTraveller,
        async function (req, res) {
            try {
                let id = req.params.id
                let driver_id = req.session.user?.driver_id

                let result = await client.query(/* sql */ `
                    select
                    count(*) as pending_order
                    from request
                    where driver_id = 4
                    and (cancel_time is null and depart_time is null)
                    `)
                let pending_order = result.rows[0].pending_order
                if (pending_order > 0) {
                    res.status(409).json({ error: 'Already has pending order' })
                    return
                }

                result = await client.query(
                /* sql */ `
                update request
                set driver_id = $1 
                where id = $2
                `,
                    [driver_id, id],
                )
                res.json(result.rows)
            } catch (error: any) {
                res.status(500).json({ error: error.toString() })
            }
        },
    )
}