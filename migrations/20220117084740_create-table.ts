import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable('gender'))) {
        await knex.schema.createTable('gender', table => {
            table.increments('id')
            table.string('gender', 256).nullable()
            table.timestamps(false, true)
        })
    }

    if (!(await knex.schema.hasTable('location'))) {
        await knex.schema.createTable('location', table => {
            table.increments('id')
            table.point('latlng').nullable()
            table.string('name').notNullable()
            table.timestamps(false, true)
        })
    }

    if (!(await knex.schema.hasTable('users'))) {
        await knex.schema.createTable('users', table => {
            table.increments('id')
            table.string('phone_num', 8).notNullable()
            table.string('password_hash', 60).notNullable()
            table.integer('gender_id').nullable()
            table.timestamps(false, true)
        })
    }

    if (!(await knex.schema.hasTable('driver'))) {
        await knex.schema.createTable('driver', table => {
            table.increments('id')
            table.integer('user_id').notNullable().references('users.id')
            table.integer('location_id').nullable().references('location.id')
            table.string('driver_license_image', 256).notNullable()
            table.string('first_name', 256).notNullable()
            table.string('last_name', 256).notNullable()
            table.timestamps(false, true)
        })
    }

    if (!(await knex.schema.hasTable('traveller'))) {
        await knex.schema.createTable('traveller', table => {
            table.increments('id')
            table.integer('user_id').notNullable().references('users.id')
            table.integer('pick_address').nullable().references('location.id')
            table.integer('drop_off_address').nullable().references('location.id')
            table.boolean('on_car').nullable()
            table.string('first_name', 256).nullable()
            table.string('last_name', 256).nullable()
            table.timestamps(false, true)
        })
    }

    if (!(await knex.schema.hasTable('request'))) {
        await knex.schema.createTable('request', table => {
            table.increments('id')
            table.integer('driver_id').nullable().references('driver.id')
            table.integer('pickup_location').notNullable().references('location.id')
            table.integer('drop_off_location').notNullable().references('location.id')
            table.integer('driver_location').nullable().references('location.id')
            table.boolean('female_only').notNullable()
            table.boolean('male_only').notNullable()
            table.integer('passenger_limit').notNullable()
            table.boolean('red_tunnel').notNullable()
            table.boolean('west_tunnel').notNullable()
            table.boolean('east_tunnel').notNullable()
            table.boolean('any_tunnel').notNullable()
            table.timestamp('planned_apart_time').notNullable()
            table.timestamp('cancel_time').nullable()
            table.timestamp('apart_time').nullable()
            table.timestamp('depart_time').nullable()
            table.timestamps(false, true)
        })
    }

    if (!(await knex.schema.hasTable('passenger'))) {
        await knex.schema.createTable('passenger', table => {
            table.increments('id')
            table.integer('request_id').notNullable().references('request.id')
            table.integer('user_id').notNullable().references('users.id')
            table.timestamps(false, true)
        })
    }

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('passenger')
    await knex.schema.dropTableIfExists('request')
    await knex.schema.dropTableIfExists('traveller')
    await knex.schema.dropTableIfExists('driver')
    await knex.schema.dropTableIfExists('users')
    await knex.schema.dropTableIfExists('location')
    await knex.schema.dropTableIfExists('gender')
}

