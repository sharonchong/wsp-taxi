
import { Application } from 'express'
import { Server } from 'socket.io'
import { client } from './db';
// import { client } from './db'
// import { checkSignInTraveller } from './guard'

export function orderRoom(app: Application, io: Server) {
    io.on('connection', socket => {
        socket.on('join-order-room', data => {
            console.log('join-order-room', data);
            socket.join('order-room:' + data.request_id)

            // io.on('onboard', message => {
            //     console.log(message)
            //     new Runner(io, data.request_id)
            // })
            setTimeout(() => {
                new Runner(io, data.request_id)
            }, 15000);
        })
    })
    // new Runner(io, 120)
}

type Point = {
    x: number
    y: number
}

class Runner {

    interval = 50

    randomness = 8 / 5

    position: Point = { x: 0, y: 0 }
    dest: Point = { x: 0, y: 0 }
    speed = 0.00008
    min_distance = 0.0005
    max_speed = 0.005

    step_to_pickup = 200

    pickup: Point = { x: 0, y: 0 }
    drop_off: Point = { x: 0, y: 0 }

    status: 'to_pickup' | 'to_drop_off' | 'finished' = 'to_pickup'

    timer: any

    constructor(
        public io: Server,
        public room_id: number,
    ) {
        this.init()
    }
    async init() {
        let result = await client.query(`select
        pickup_location.latlng pickup_location_latlng
        , drop_off_location.latlng drop_off_location_latlng
        
        from request
        inner join location pickup_location on request.pickup_location = pickup_location.id
        inner join location drop_off_location on request.drop_off_location = drop_off_location.id
        where request.id = $1;`, [this.room_id])
        let address = result.rows[0]
        let { pickup_location_latlng, drop_off_location_latlng } = address
        this.pickup = pickup_location_latlng
        this.drop_off = drop_off_location_latlng


        // random() = 0 .. 1
        // random() * 2 = 0 .. 2
        // random() * 2 - 1 = -1 .. 1

        this.position = {
            x: this.pickup.x + (Math.random() * 2 - 1) * this.speed * this.step_to_pickup,
            y: this.pickup.y + (Math.random() * 2 - 1) * this.speed * this.step_to_pickup,
        }
        this.dest = this.pickup

        this.status = 'to_pickup'
        this.io.to('order-room:' + this.room_id).emit('status-changed', this.status)
        this.timer = setInterval(() => {
            this.loop()
        }, this.interval)
    }
    loop() {
        let dx = this.dest.x - this.position.x
        let dy = this.dest.y - this.position.y

        if (Math.abs(dx) <= this.min_distance && Math.abs(dy) < this.min_distance) {
            if (this.dest == this.pickup) {
                this.dest = this.drop_off
                this.status = 'to_drop_off'
                this.io.to('order-room:' + this.room_id).emit('status-changed', this.status)
            } else {
                // arrived

                this.status = 'finished'
                this.io.to('order-room:' + this.room_id).emit('status-changed', this.status)
                clearInterval(this.timer)


                // this.status = 'to_pickup'
                // this.io.to('order-room:' + this.room_id).emit('status-changed', this.status)
                // this.dest = this.pickup



                return
            }
        }


        dx += (Math.random() * 2 - 1) * dx * this.randomness
        dy += (Math.random() * 2 - 1) * dy * this.randomness

        if (Math.abs(dx) > this.max_speed) {
            dx = Math.sign(dx) * this.max_speed
        }
        if (Math.abs(dy) > this.max_speed) {
            dy = Math.sign(dy) * this.max_speed
        }


        this.position.x += dx * 1 / 10
        this.position.y += dy * 1 / 10
        console.log({
            dest: this.dest,
            pos: this.position,
            speed: { dx, dy }
        });

        // this.position.x += (Math.random() * 2 - 1) * this.speed
        // this.position.y += (Math.random() * 2 - 1) * this.speed
        this.io.to('order-room:' + this.room_id).emit('driver-move', this.position)


    }
}