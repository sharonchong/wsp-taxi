import { Request, Response, NextFunction } from "express";
import "./session";

export function checkRegister(req: Request, res: Response, next: NextFunction) {
  if (req.session.user) {
    next();
  } else {
    res.redirect("/register.html");
  }
}

export function checkSignInDriver(
  req: Request,
  res: Response,
  next: NextFunction
) {
  // if (!req.session.user) {
  //   req.session.user = {
  //     is_driver: true,
  //     driver_id: 4,
  //     user_id: 4,
  //     phone_num: 98765432,
  //   };
  // }
  if (req.session.user?.driver_id) {
    next();
  } else {
    // res.redirect('/register.html')
    res
      .status(403)
      .json({ error: "This function is only available to driver" });
  }
}

export function checkSignInTraveller(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (req.session.user?.traveller_id) {
    next();
  } else {
    // res.redirect('/register.html')
    res
      .status(403)
      .json({ error: "This function is only available to traveller" });
  }
}

export function checkSignInNotTraveller(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (req.session.user && !req.session.user.traveller_id) {
    next();
  } else {
    // res.redirect('/register.html')
    res
      .status(403)
      .json({ error: "This function is only available to traveller" });
  }
}
