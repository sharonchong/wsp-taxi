let onboardButton = document.querySelector('#onboardButton')
let arrivedBtn = document.querySelector('#arrivedButton')
let orderNode = document.querySelector('.order')
let request_id

let socket = io.connect()

onboardButton.addEventListener('click', function onClick() {
  onboardButton.textContent = 'passenger on board!'
  onboardButton.setAttribute('disabled', '')
  // socket.emit('onboard','onboard')
})

arrivedBtn.addEventListener('click', async function () {
  try {
    let res = await fetch(`/driverOrder/${request_id}/depart`, {
      method: 'POST',
    })
    let json = await res.json()
    console.log(json)
    if (json.error) {
      // TODO show error
      return
    }
    window.location = '/driverOrder.html'
  } catch (error) {
    // TODO show error
  }
})

async function loadOrder() {
  try {
    let res = await fetch(`/driverOrder/current`)
    let json = await res.json()
    console.log(json)
    if (json.error) {
      if (json.error == 'No active order') {
        window.location = '/driverOrder.html'
        return
      }
      // TODO show error
      return
    }
    request_id = json.request_id
    orderNode.querySelector('.pickup_location_name').textContent =
      json.pickup_location_name
    orderNode.querySelector('.drop_off_location_name').textContent =
      json.drop_off_location_name
  } catch (error) {
    // TODO show error
  }
}
loadOrder()

socket.on('connect', () => {
  console.log('connected', socket.id);
})

