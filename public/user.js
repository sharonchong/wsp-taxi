// function createOption(optionJson) {
//     for (let select of optionJson) {
//         let option = document.createElement('option');
//         option.value = select.id;
//         option.textContent = select.name
//         favAddressPickup.append(option)
//     }
// }

window.onload = async function () {
    try {
        let res = await fetch('/user')
        let json = await res.json();
        if (json.error) {
            if (json.error == 'This function is only available to traveller') {
                location.href = '/signIn.html'
            }
            // TODO show error message
            return
        }
    } catch (e) {
        console.log(e)
    }
}

requirementForm.addEventListener('submit', async function (event) {
    event.preventDefault()
    let form = event.target

    try {
        if (pickRound == 2) {
            let res = await fetch("/user/requirements", {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    pickup: addressObj.pickup,
                    dropOff: addressObj.dropOff,
                    female: form.female.checked,
                    male: form.male.checked,
                    passengerLimit: passengerLimit.value,
                    redTunnel: form.redTunnel.checked,
                    westTunnel: form.westTunnel.checked,
                    eastTunnel: form.eastTunnel.checked,
                    anyTunnel: form.anyTunnel.checked,
                })
            })

            let json = await res.json();

            
            Swal.fire({
                title: json.message,
                text: `Request ID: ${json.request}`,
                icon: 'success',
            })
            setTimeout(() => {
                window.location = `/orderRoom.html?requestId=${json.request}`
            }, 1500);
        } else {
            Swal.fire({
                title: 'Failed to create order',
                text: 'Please select pickup and drop off location',
                icon: 'error',
            })
        }
    } catch (e) {
        console.log(e)
    }
})

const joinRoom = document.querySelector('.joinRoom')
joinRoom.addEventListener('click', async event => {
    window.location = 'joinRoom.html'
})