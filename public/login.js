let signInForm = document.querySelector('#signIn')

signInForm.addEventListener('submit', async event => {
    event.preventDefault();
    let form = signInForm
    const phoneNum = form.phone.value
    const password = form.password.value
    const formObject = { phoneNum, password }
    try {
        let res = await fetch(form.action, {
            method: form.method,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formObject)
        })
        let json = await res.json();

        if (200 <= res.status && res.status < 300) {
            Swal.fire({
                title: 'Success to Login',
                text: json.message,
                icon: 'success',
            })
            setTimeout(() => {
                if (json.is_driver) {
                    window.location = '/driverOrder.html'
                } else if (json.is_traveller) {
                    window.location = '/user.html'
                } else {
                    window.location = '/select.html'
                }
            }, 2000)
        } else {
            Swal.fire({
                title: 'Failed to Login',
                text: json.message,
                icon: 'error',
            })
        }


    } catch (error) {
        console.log(error)
        Swal.fire({
            title: 'Failed to Login',
            text: json.message,
            icon: 'error',
        })
    }
})

// function devMode() {
//     // driver
//     signInForm.phone.value = '55555555'
//     signInForm.password.value = '12345678'

//     // passenger
//     signInForm.phone.value = '66666666'
// }
// devMode()