const registerButton = document.querySelector('.signUp')
const loginIn = document.querySelector('.loginIn')
const buttonLeft = document.querySelector('.buttonLeft')

registerButton.addEventListener('click', function(e) {
    window.location = '/register.html'
})
loginIn.addEventListener('click', function(e) {
    window.location = '/signIn.html'
})
buttonLeft.addEventListener('click', function(e) {
    window.location = '/'
})