// //document
// const acceptList =  document.querySelectorAll(".acceptBtn")

// for(let acceptBtn of acceptList)
//   acceptBtn.addEventListener("click",  function (event) {

//     Swal.fire({
//         title: '確認接單？',
//         // text: "You won't be able to revert this!",
//         icon: 'warning',
//         showCancelButton: true,
//         confirmButtonColor: '#228c22',
//         cancelButtonColor: '#d33',
//         confirmButtonText: '係 ',
//         cancelButtonText: '唔係'

//       }).then(async (result) => {
//         if (result.isConfirmed) {

//           Swal.fire(
//             '成功!',
//             '你已成功接單',
//             'success'
//           )

//           fetch()

//           window.location = '/pickedOrder.html'

//         }
//       }).then(function() {

//       })

//   });

const orderContainer = document.getElementById("orderContainer");
const orderTemplate = orderContainer.querySelector(".card");
const noOrderDiv = document.querySelector('.noOrder')
orderTemplate.remove();
let socket = io.connect()
function tunnel(order) {
  if (order.red_tunnel === true) {
    return "红隧";
  } else if (order.west_tunnel === true) {
    return "西隧";
  } else if (order.east_tunnel === true) {
    return " 東隧";
  } else if (order.any_tunnel === true) {
    return "任何隧道";
  } else return " ";
}

async function confirmAcceptOrder(id) {
  let result = await Swal.fire({
    title: "確認接單？",
    // text: "You won't be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#228c22",
    cancelButtonColor: "#d33",
    confirmButtonText: "係 ",
    cancelButtonText: "唔係",
  });
  if (!result.isConfirmed) {
    return;
  }

  let res = await fetch(`/driverOrder/${id}/pickOrder`, {
    method: "POST",
  });
  let json = await res.json();
  if (json.error) {
    Swal.fire("Failed to 接單", json.error, "error");
    return;
  }
  Swal.fire("成功!", "你已成功接單", "success");
  setTimeout(() => {
    window.location = "/pickedOrder.html";
  }, 500);
}

async function loadDriverOrder() {
  let res = await fetch("/driverOrder");
  let json = await res.json();
  if(json.error == 'This function is only available to driver'){
    location.href = 'signIn.html'
    return
  }
  if(json.length == 0){
    noOrderDiv.removeAttribute('hidden','')
  }
  for (let order of json) {
    console.log(order);
    let orderNode = orderTemplate.cloneNode(true);
    orderNode.querySelector(".passenger_limit").textContent =
      order.passenger_limit;
    orderNode.querySelector(".pickup_location_name").textContent =
      order.pickup_location_name;
    orderNode.querySelector(".drop_off_location_name").textContent =
      order.drop_off_location_name;
    orderNode.querySelector(".tunnel").textContent = tunnel(order);
    orderNode.querySelector(".acceptBtn").addEventListener("click", () => {
      confirmAcceptOrder(order.request_id);
    });
    orderContainer.appendChild(orderNode);
  }
}
loadDriverOrder();

async function getDriverOrder() {
  let res = await fetch("/driverOrder");
  let result = await res.json();
  return result;
}

async function displayOrder() {
  let result = await getDriverOrder();

  console.log(result);

  let str = ``;
  for (let val of result) {
    str += `
    
    <div class="card" style="width: 20rem;">
    <div class="card-body">
    <h3>${val.passenger_limit}位乘客 </h3>
   

        <h6 class="card-title">${val.pickup_location_name}</h6>
        <p>去</p>
        <h6 class="card-title">${val.drop_off_location_name}</h6>
        
        <p class="card-subtitle mb-2 text-muted">${tunnel(val)}</p>
        <h4>
            
        </h4>
        <div class="row">
            <button type="button" class="acceptBtn btn btn-primary btn-lg">Accept</button>
        </div>
    </div>
    </div>
    `;
  }

  orderContainer.innerHTML += str;

  function tunnel(val) {
    if (val.red_tunnel === true) {
      return "红隧";
    } else if (val.west_tunnel === true) {
      return "西隧";
    } else if (val.east_tunnel === true) {
      return " 東隧";
    } else if (val.any_tunnel === true) {
      return "任何隧道";
    } else return " ";
  }
  //document
  const acceptList = document.querySelectorAll(".acceptBtn");

  for (let acceptBtn of acceptList)
    acceptBtn.addEventListener("click", function (event) {
      Swal.fire({
        title: "確認接單？",
        // text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#228c22",
        cancelButtonColor: "#d33",
        confirmButtonText: "係 ",
        cancelButtonText: "唔係",
      })
        .then(async (result) => {
          if (result.isConfirmed) {
            Swal.fire("成功!", "你已成功接單", "success");

            let res = await fetch(`/driverOrder/${id}/pickOrder`, {
              method: "POST",
            });
            let json = await res.json();
            if (json.error) {
              return;
            }

            window.location = "/pickedOrder.html";
          }
        })
        .then(function () {});
    });
}

// displayOrder();

// <li class="card-text">${passenger_limit}</li>

socket.on('request', request => {
  console.log(request)
  noOrderDiv.setAttribute('hidden','')
  let orderNode = orderTemplate.cloneNode(true);
      orderNode.querySelector(".passenger_limit").textContent =
          request.passenger_limit;
      orderNode.querySelector(".pickup_location_name").textContent =
          request.pickup.name;
      orderNode.querySelector(".drop_off_location_name").textContent =
          request.dropOff.name;
      orderNode.querySelector(".tunnel").textContent = tunnel(request);
      orderNode.querySelector(".acceptBtn").addEventListener("click", () => {
          confirmJoinOrder(request.request_id);
      });
      orderContainer.appendChild(orderNode);
})