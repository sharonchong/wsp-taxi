let text = `Come join FAKE TAXI, we know you like it~`
let textIndex = 0
let textCursor

function tickText() {
    if (textIndex >= text.length) {
        // textCursor = document.createElement('div')
        textCursor.classList.add('cursor')
        textCursor.textContent = '|'
        textOutput.appendChild(textCursor)
        setTimeout(tickCursor, 60)
        return
    }
    let span = document.createElement('span')
    span.textContent = text[textIndex]
    textIndex++
    textOutput.appendChild(span)
    setTimeout(tickText, 60)
}
setTimeout(tickText, 1000)

function tickCursor() {
    textCursor.classList.toggle('active')
    setTimeout(tickCursor, 600)
}

window.onload = async () =>{

    let res = await fetch("/checkUserIsLogin");
    let result = await res.json();

    console.log(result)

    if(result.login){

        if(result.role === 'driver'){
            location.href = "/driverOrder.html" 
        }
        else if(result.role === 'traveller'){
            location.href = "/user.html" 
        }
        else{
            location.href = "/select.html" 
        }

    } 

}

