let driverForm = document.querySelector('#driverProfileForm')

let driverData;

driverForm.addEventListener('submit', async event => {
    event.preventDefault();
    let form = driverForm

    console.log(form.license.files)
    let body = new FormData(form)
    console.log(body.license)

    console.log(form.license.files.length)

    if(form.license.files.length === 0){
        alert("no images!!!!")
        return;
    }


    if(!driverData.driver_license_image && form.license.files.length === 0){
        alert("no images!!!!")
        return;
    }



    // if(form.license.files.length==0) {
    //     //pop alert
    //     return
    // }
//     if(res.status==403){
//         setTimeout(() =>{Swal.fire({
//             title: 'Please Upload a license file',
//             text: json.message,
//             icon: 'error',
//           })
// },2000)
        
//     }

    try {
        let res = await fetch(form.action, {
            method: form.method,
            body,
        })
        let json = await res.json()
        
    } catch (error) {
        console.log(error)
    }
    finally { window.location = '/driverOrder.html' }

}
)
const cancelButton = document.querySelector('#cancelButton')
cancelButton.addEventListener('click', function (event) {
    event.preventDefault();
    window.location = '/index.html'
})




window.addEventListener('load', async () => {

    try {
        let res = await fetch('/profile/driver')
        let json = await res.json();
        console.log(json);

        if (json.error) {
            if (json.error == 'This function is only available to driver') {
                location.href = '/signIn.html'
            }
            // TODO show error message
            return
        }

        driverData = json;

        console.log(driverData)

        driverForm.querySelector('.profileUserID').value = json.user_id
        driverForm.first_name.value = json.first_name
        driverForm.last_name.value = json.last_name
        driverForm.phone_num.value = json.phone_num
        console.log(driverForm.uploadLicense.value)

        // driverForm.license.value = json.driver_license_image

        if (json.driver_license_image) {
            driverForm.querySelector('img').src = '/uploads/' + json.driver_license_image
            driverForm.driver_license_image.value = json.driver_license_image
        }
        else {
            driverForm.querySelector('img').src = '/doc/blank01.jpeg'
        }
    }
    catch (error) {
        console.log(error)
    }

})

// setTimeout(() =>{
//     window.location='/driver.html'
//   },2000)

const registerButton = document.querySelector('.signUp')
const loginIn = document.querySelector('.loginIn')
const buttonLeft = document.querySelector('.buttonLeft')

registerButton.addEventListener('click', function (e) {
    window.location = '/driverOrder.html'
})
const logoutButton = document.querySelector('#logout')
logoutButton.addEventListener('click', async event => {
    try {
        let res = await fetch('/user/logout')
        window.location = 'signIn.html'
    } catch (e) {
        console.error
    }
})