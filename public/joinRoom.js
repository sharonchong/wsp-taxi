const orderContainer = document.getElementById("orderContainer");
const orderTemplate = orderContainer.querySelector(".card");
const noOrderDiv = document.querySelector('.noOrder')
orderTemplate.remove();

let socket = io.connect()

socket.on('request', request => {
    console.log(request)
    noOrderDiv.setAttribute('hidden', '')
    let orderNode = orderTemplate.cloneNode(true);
    orderNode.querySelector(".passenger_limit").textContent =
        request.passenger_limit;
    orderNode.querySelector(".pickup_location_name").textContent =
        request.pickup.name;
    orderNode.querySelector(".drop_off_location_name").textContent =
        request.dropOff.name;
    orderNode.querySelector(".tunnel").textContent = tunnel(request);
    orderNode.querySelector(".acceptBtn").addEventListener("click", () => {
        confirmJoinOrder(request.request_id);
    });
    orderContainer.appendChild(orderNode);
})

function tunnel(order) {
    if (order.red_tunnel === true) {
        return "红隧";
    } else if (order.west_tunnel === true) {
        return "西隧";
    } else if (order.east_tunnel === true) {
        return " 東隧";
    } else if (order.any_tunnel === true) {
        return "任何隧道";
    } else return " ";
}

async function confirmJoinOrder(id) {
    let result = await Swal.fire({
        title: "確認加入？",
        // text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#228c22",
        cancelButtonColor: "#d33",
        confirmButtonText: "係 ",
        cancelButtonText: "唔係",
    });
    if (!result.isConfirmed) {
        return;
    }

    let res = await fetch(`/joinOrder/${id}/pickOrder`, {
        method: "POST",
    });
    let json = await res.json();
    if (json.error) {
        Swal.fire("Failed to join", json.error, "error");
        return;
    }
    Swal.fire("成功!", "你已成功加入", "success");
    setTimeout(() => {
        window.location = `/orderRoom.html?requestId=${id}`;
    }, 500);
}

async function loadOrder() {
    let res = await fetch("/joinOrder");
    let json = await res.json();
    if (json.error) {
        console.log(json.error);
        if (res.status == 403) {
            location.href = '/signIn.html'
        }
        return;
    }
    if (json.length == 0) {
        noOrderDiv.removeAttribute('hidden', '')
    }
    for (let order of json) {
        let orderNode = orderTemplate.cloneNode(true);
        orderNode.querySelector(".passenger_limit").textContent =
            order.passenger_limit;
        orderNode.querySelector(".pickup_location_name").textContent =
            order.pickup_location_name;
        orderNode.querySelector(".drop_off_location_name").textContent =
            order.drop_off_location_name;
        orderNode.querySelector(".tunnel").textContent = tunnel(order);
        orderNode.querySelector(".acceptBtn").addEventListener("click", () => {
            confirmJoinOrder(order.request_id);
        });
        orderContainer.appendChild(orderNode);
    }
}
loadOrder();

async function getOrder() {
    let res = await fetch("/joinOrder");
    let result = await res.json();
    return result;
}

async function displayOrder() {
    let result = await getOrder();

    let str = ``;
    for (let val of result) {
        str += `
        
        <div class="card" style="width: 20rem;">
        <div class="card-body">
        <h3>${val.passenger_limit}位乘客 </h3>
        

        <h6 class="card-title">${val.pickup_location_name}</h6>
        <p>去</p>
        <h6 class="card-title">${val.drop_off_location_name}</h6>
        
        <p class="card-subtitle mb-2 text-muted">${tunnel(val)}</p>
        <h4>
        
        </h4>
        <div class="row">
        <button type="button" class="acceptBtn btn btn-primary btn-lg">Accept</button>
        </div>
        </div>
        </div>
        `;
    }

    orderContainer.innerHTML += str;

    function tunnel(val) {
        if (val.red_tunnel === true) {
            return "红隧";
        } else if (val.west_tunnel === true) {
            return "西隧";
        } else if (val.east_tunnel === true) {
            return " 東隧";
        } else if (val.any_tunnel === true) {
            return "任何隧道";
        } else return " ";
    }
    //document
    const acceptList = document.querySelectorAll(".acceptBtn");

    for (let acceptBtn of acceptList)
        acceptBtn.addEventListener("click", function (event) {
            Swal.fire({
                title: "確認加入？",
                // text: "You won't be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#228c22",
                cancelButtonColor: "#d33",
                confirmButtonText: "係 ",
                cancelButtonText: "唔係",
            })
                .then(async (result) => {
                    if (result.isConfirmed) {
                        Swal.fire("成功!", "你已成功加入", "success");

                        let res = await fetch(`/joinOrder/${id}/pickOrder`, {
                            method: "POST",
                        });
                        let json = await res.json();
                        if (json.error) {
                            return;
                        }

                        window.location = `/orderRoom.html?requestId=${id}`;
                    }
                })
                .then(function () { });
        });
}

const logoutButton = document.querySelector('#logout')
logoutButton.addEventListener('click', async event => {
    try {
        let res = await fetch('/user/logout')
        window.location = 'signIn.html'
    } catch (e) {
        console.error
    }
})

const travellerProfileButton = document.querySelector('#driverProfile')
travellerProfileButton.addEventListener('click', event => {
    window.location = 'traveller.html'
})

const titleButton = document.querySelector('.buttonLeft')
titleButton.addEventListener('click', event => {
    window.location = 'user.html'
})