let logoutButton = document.querySelector('#logout')
logoutButton.addEventListener('click', async event => {
    
    try{
        let res=await fetch('/user/logout')
        let json
        window.location='signIn.html'
    }catch(e){
        console.error
    }
})
let driverProfileButton = document.querySelector('#driverProfile')
driverProfileButton.addEventListener('click', async event => {
    window.location='driver.html'
    try{
        let res=await fetch('/profile/driver')
        let json= await res.json();
    }catch(e){
        console.log(e)
    }
})

let travellerProfileButton = document.querySelector('#travellerProfile')
travellerProfileButton.addEventListener('click', async event => {
    window.location='traveller.html'
})

