let registerForm = document.querySelector('#register')

registerForm.addEventListener('submit', async event => {
    event.preventDefault();
    let form = registerForm
    
    try{
        let res = await fetch(form.action, {
            method: form.method,
            
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "phoneNum": form.phone.value,
                "password": form.password.value,
                "matchPassword":form.matchPassword.value
            }),
        })
        let json= await res.json();
        
        if (200 <= res.status && res.status < 300) {
            Swal.fire({
              title: 'Success to Register',
              text: json.message,
              icon: 'success',
            })
            setTimeout(() =>{
              window.location='/select.html'
            },2000)
            
          } else {
            Swal.fire({
              title: 'Failed to Register',
              text: json.message,
              icon: 'error',
            })
          }
        }
     catch (error) {
        console.log(error)
        Swal.fire({
            title: 'Failed to Register',
            text: json.message,
            icon: 'error',
          })
    }
})
const signInButton = document.querySelector('#SignInButton')

signInButton.addEventListener('click', function(e) {
  window.location='/signIn.html'
})