// map logic
const gpsButton = document.querySelector('#gpsButton')
const favAddressPickup = document.querySelector('.favAddressPickup')
const pickupInput = document.querySelector('.pickupInput')
const dropOffInput = document.querySelector('.dropOffInput')
const pickConfirm = document.querySelector('.pickConfirm')
const dropConfirm = document.querySelector('.dropConfirm')
const pickTick = document.querySelector('.pickTick')
const dropTick = document.querySelector('.dropTick')
const requirementForm = document.querySelector('#requirementForm')
const passengerLimit = document.querySelector('.passengerLimit')

let map = L.map('map').setView([22.3608704, 114.1374842], 10);

let osm = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYnJpYW5rd2xvIiwiYSI6ImNreDl1a3p6YjB6N2Uyd3J5cXN5aDJ0ZHAifQ.qn84CWQg_iRt-u1AsCktCQ'
})
osm.addTo(map);

if (!navigator.geolocation) {
    console.log('Your browser')
} else {
    gpsButton.addEventListener('click', function (event) {
        navigator.geolocation.getCurrentPosition(getPosition)
    })
}


let marker, circle

//get the current location in map
function getPosition(position) {
    let lat = position.coords.latitude
    let long = position.coords.longitude
    let accuracy = position.coords.accuracy

    if (marker) {
        map.removeLayer(marker)
    }

    if (circle) {
        map.removeLayer(circle)
    }

    marker = L.marker([lat, long]).addTo(map);
    circle = L.circle([lat, long], (accuracy))

    let featureGroup = L.featureGroup([marker, circle]).addTo(map)

    map.fitBounds(featureGroup.getBounds());
}

//use gps to locate current address to the pickup location
function getPositionIncludeGPS(position) {
    let lat = position.coords.latitude
    let long = position.coords.longitude
    let accuracy = position.coords.accuracy

    if (marker) {
        map.removeLayer(marker)
    }

    if (circle) {
        map.removeLayer(circle)
    }

    marker = L.marker([lat, long]).addTo(map);
    circle = L.circle([lat, long], (accuracy))

    let featureGroup = L.featureGroup([marker, circle]).addTo(map)

    map.fitBounds(featureGroup.getBounds());

    targetThis.options.target = `${lat} , ${long}`
    targetThis.addTo(map);
}


//Route logic
// let targetThis = L.leafletControlRoutingtoaddress({
//     position: 'topright',
//     router: 'mapbox',
//     token: 'pk.eyJ1IjoiYnJpYW5rd2xvIiwiYSI6ImNreDl1a3p6YjB6N2Uyd3J5cXN5aDJ0ZHAifQ.qn84CWQg_iRt-u1AsCktCQ',
//     placeholder: 'Please insert your address here.',
//     errormessage: 'Address not valid.',
//     distance: 'Distance:',
//     duration: 'Time need:',
//     target: 'Kwai Chung, Hong Kong',
//     requesterror: '"Too Many Requests" or "Not Authorized - Invalid Token"'

// })

// targetThis.addTo(map);

favAddressPickup.addEventListener('change', async function (event) {
    if (event.target.value == 'gps') {
        navigator.geolocation.getCurrentPosition(getPositionIncludeGPS)
    }
})

let pickRound = 0;
let addressObj = {};
let routeArray = new Array();
map.on('click', async function (e) {
    if (marker) {
        map.removeLayer(marker)
    }
    let googleRes = await fetch('/google/address', {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
            "lat": e.latlng.lat,
            "lng": e.latlng.lng
        })
    })
    let json = await googleRes.json()
    if (pickRound == 0) {
        pickupInput.value = json.address
        pickConfirm.removeAttribute('hidden')
        pickConfirm.addEventListener('click', async function (e) {
            try {
                pickupInput.setAttribute('readonly', '')
                pickConfirm.setAttribute('hidden', '')
                pickTick.removeAttribute('hidden')
                pickRound = 1
                addressObj.pickup = {
                    lat: json.lat,
                    lng: json.lng,
                    address: json.address
                }
                // let res = await fetch('/user/location',{
                //     method: 'POST',
                //     headers: {"Content-Type": "application/json"},
                //     body: JSON.stringify({
                //         "lat": json.lat,
                //         "lng": json.lng,
                //         "address": json.address
                //     })
                // })
                // let idJson = await res.json()
                // pickupInput.setAttribute('id', `location${idJson.id.id}`)
            } catch (e) {
                console.log(e)
            }
        })
    } else if (pickRound == 1) {
        dropOffInput.value = json.address
        dropConfirm.removeAttribute('hidden')
        dropConfirm.addEventListener('click', async function (e) {
            try {
                dropOffInput.setAttribute('readonly', '')
                dropConfirm.setAttribute('hidden', '')
                dropTick.removeAttribute('hidden')
                pickRound = 2
                addressObj.dropOff = {
                    lat: json.lat,
                    lng: json.lng,
                    address: json.address
                }

                let routeControl = L.Routing.control({
                    waypoints: [
                        L.latLng(addressObj.pickup.lat, addressObj.pickup.lng),
                        L.latLng(addressObj.dropOff.lat, addressObj.dropOff.lng)
                    ]
                }).addTo(map);

            } catch (e) {
                console.log(e)
            }
        })
    }
    if (pickRound != 2) {
        marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
        marker.bindPopup(`<p>${json.address}</p>`).openPopup();
    }
});

const getCenterGPS = document.querySelector('#getCenterGPS')
getCenterGPS.addEventListener('click', function (e) {
    let location = map.getCenter()
    if (marker) {
        map.removeLayer(marker)
    }
    marker = L.marker([location.lat, location.lng]).addTo(map)
    marker.bindPopup(`<p>lat: ${location.lat.toFixed(6)}<br/>lng: ${location.lng.toFixed(6)}</p>`).openPopup();
})