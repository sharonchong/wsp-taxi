let params = location.search
let searchParams = new URLSearchParams(location.search);
let id = searchParams.get('requestId');
console.log(id)
let socket = io.connect()


let map = L.map('map').setView([22.3608704, 114.1374842], 10);

let osm = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYnJpYW5rd2xvIiwiYSI6ImNreDl1a3p6YjB6N2Uyd3J5cXN5aDJ0ZHAifQ.qn84CWQg_iRt-u1AsCktCQ'
})
osm.addTo(map);

console.log('get gps');

let marker
let selfMarker
let driverMarker
let runner

function onPosition(position) {
    let lat = position.coords.latitude
    let lng = position.coords.longitude
    console.log('got position:', { lat, lng });
    if (!selfMarker) {
        // selfMarker = new L.Marker({ lat, lng })
        // selfMarker.addTo(map)
        // runner = new Runner(marker)
        selfMarker = createMarker({ position: { lat, lng } })
    }
    selfMarker.setLatLng({ lat, lng });
}
function onPositionError(error) {
    console.error('failed to get position:', error);
}
navigator.geolocation.getCurrentPosition(onPosition, onPositionError, {
    enableHighAccuracy: false
})
navigator.geolocation.watchPosition(onPosition, onPositionError, {
    enableHighAccuracy: true
})

class Runner {
    constructor(marker, interval = 1000, speed = 0.005, direction = { lat: 0.4, lng: 0.4 }, endTime = 10000) {
        this.marker = marker
        this.interval = interval
        this.speed = speed
        this.direction = direction
        this.zoom = 14
        this.endTime = endTime
        this.start()
        this.stop()
    }
    start() {
        this.timer = setInterval(() => this.run(), this.interval)
    }
    run() {
        let latLng = this.marker.getLatLng()
        latLng.lat += (Math.random() * 2 - 1 + this.direction.lat) * this.speed
        latLng.lng += (Math.random() * 2 - 1 + this.direction.lng) * this.speed
        this.marker.setLatLng(latLng)
        map.setView(latLng, this.zoom)
    }
    stop() {
        setInterval(() => {
            clearInterval(this.timer)
        }, this.endTime);
    }
}

async function loadOrderRoute() {
    try {
        let res = await fetch('/user/orderRoute?request_id=' + id)
        if (res.status == 403) {
            location.href = 'signIn.html'
            return
        }
        let json = await res.json()
        console.log('pickup', json.pickup_location_latlng);
        createMarker({ position: json.pickup_location_latlng, iconSize: [38, 95], iconUrl: 'https://leafletjs.com/examples/custom-icons/leaf-green.png' })
        createMarker({ position: json.drop_off_location_latlng, iconSize: [38, 95], iconUrl: 'https://leafletjs.com/examples/custom-icons/leaf-red.png' })
        let routeControl = L.Routing.control({
            waypoints: [
                L.latLng(json.pickup_location_latlng.x, json.pickup_location_latlng.y),
                L.latLng(json.drop_off_location_latlng.x, json.drop_off_location_latlng.y)
            ]
        }).addTo(map);
    } catch (error) {
        console.log(error)
    }
}
loadOrderRoute()

const titleButton = document.querySelector('.buttonLeft')
titleButton.addEventListener('click', event => {
    window.location = 'user.html'
})

const travellerProfileButton = document.querySelector('.setting')
travellerProfileButton.addEventListener('click', event => {
    window.location = 'traveller.html'
})

const logoutButton = document.querySelector('#logout')
logoutButton.addEventListener('click', async event => {
    try {
        let res = await fetch('/user/logout')
        window.location = 'signIn.html'
    } catch (e) {
        console.error
    }
})

const driverID = document.querySelector('.driverID')

socket.on('connect', () => {
    console.log('connected', socket.id);
    socket.emit('join-order-room', { request_id: id })
})

socket.on('driverId', id => {
    driverID.textContent = id
})

socket.on('done', message => {
    if (message == 'done') {
        Swal.fire({
            title: "目的地已到達",
            // text: "You won't be able to revert this!",
            text: '多謝乘搭!',
            icon: "warning",
            showCancelButton: false,
            confirmButtonColor: "#228c22",
            cancelButtonColor: "#d33",
        });
        setTimeout(() => {
            window.location = 'user.html'
        }, 1500)
    }
})

socket.on('driver-move', data => {
    console.log('driver-move:', data);


    if (!driverMarker) {
        var icon = L.icon({
            iconUrl: '/doc/favicon.ico',
            iconSize: [32, 32],
        });
        driverMarker = new L.Marker([0, 0], { icon })
        driverMarker.addTo(map)
        // runner = new Runner(marker)
    }

    driverMarker.setLatLng([data.x, data.y])
    map.setView([data.x, data.y])
})

const orderStatus = document.querySelector('.orderStatus')
socket.on('status-changed', data => {
    if(data == 'to_pickup'){
        orderStatus.textContent = 'To pickup'
    }
    if(data == 'to_drop_off'){
        orderStatus.textContent = 'To drop off'
    }
    if(data == 'finished'){
        orderStatus.textContent = 'Finish'
    }
})

function createMarker({ position, iconUrl, iconSize }) {
    if (position.x) {
        position = [position.x, position.y]
    }
    let icon = iconUrl ? L.icon({
        iconUrl,
        iconSize
    }) : null;
    let marker = new L.Marker(position, icon ? { icon } : undefined)
    marker.addTo(map)
    return marker
}