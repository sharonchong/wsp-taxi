let travellerForm = document.querySelector('#travellerProfileForm')
travellerForm.addEventListener('submit', async event => {
    event.preventDefault();
    let form = travellerForm
    // let body = new FormData(form)
    try {
        let res = await fetch(form.action, {
            method: form.method,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "first_name": form.first_name.value,
                "last_name": form.last_name.value,
                "phone_num": form.phone_num.value
            }),
        })
        let json = await res.json()
        setTimeout(() => {
            window.location = '/user.html'
        }, 2000)
    } catch (error) {
        console.log(error)
    }
    window.location='/user.html'
})
const cancelButton = document.querySelector('#cancelButton')
cancelButton.addEventListener('click',function (event) {
    event.preventDefault();
    window.location = '/index.html'
})

// let travellerID = document.querySelector('.travellerID')
// let travellerPhoneNum = document.querySelector('.inputPhoneNum')

window.addEventListener('load', async () => {
    try {
        let res = await fetch('/profile/traveller')
        let json = await res.json();
        console.log(json);
        if (json.error) {
            if (json.error == 'This function is only available to traveller') {
                location.href = '/signIn.html'
            }
            // TODO show error message
            return
        }
        travellerForm.querySelector('.travellerID').value = json.user_id
        travellerForm.first_name.value = json.first_name
        travellerForm.last_name.value = json.last_name
        travellerForm.phone_num.value = json.phone_num
        travellerForm.pickup_address=json.pickup_address
        travellerForm.drop_off_address=json.drop_off_address
    } catch (error) {
        console.log(error)
    }
})

const registerButton = document.querySelector('.signUp')
const loginIn = document.querySelector('.loginIn')
const buttonLeft = document.querySelector('.buttonLeft')
const driverButton = document.querySelector('#driverButton')
const passengerButton = document.querySelector('#passengerButton')

registerButton.addEventListener('click', function(e) {
    window.location = '/user.html'
})
const logoutButton = document.querySelector('#logout')
    logoutButton.addEventListener('click', async event => {
        try{
            let res=await fetch('/user/logout')
            window.location='signIn.html'
        }catch(e){
            console.error
        }
    })